import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Order, Product } from '../entities';
import { prepare } from './token';


export const orderAPI = createApi({
    reducerPath: 'orderAPI',
    tagTypes: ['OrderList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/order', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllOrders: builder.query<Order[], void>({
            query: () =>  '/',
            providesTags: ['OrderList']
        }),
        getOneOrder: builder.query<Order, number>({
            query: (id) => '/'+id
        }),
        postOrder: builder.mutation<Order, Product[]>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['OrderList']
        }),
        patchOrder: builder.mutation< Order, {body: Order, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['OrderList']
        }),
        deleteOrder: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['OrderList']
        })

    })
});


export const {useGetAllOrdersQuery, usePostOrderMutation, usePatchOrderMutation, useDeleteOrderMutation, useGetOneOrderQuery} = orderAPI;


