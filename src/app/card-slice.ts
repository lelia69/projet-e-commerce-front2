import { createSlice } from "@reduxjs/toolkit";
import { Product } from "../entities";


export interface CartSate{

    cart: Product[]
}


 export const cardSlice = createSlice({

    name: 'cartslice',
    initialState : {cart : []} as CartSate,
    reducers : {

        addCart(state, {payload}){
            state.cart.push(payload)

        },
        replaceCart(state, {payload}){
            state.cart= payload
        },

        deleteCart(state, {payload}){
            state.cart.splice(payload)
        }
    }
});


export const { addCart, deleteCart, replaceCart } = cardSlice.actions


export default cardSlice.reducer

