import { RootState } from './store';
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '../entities'
import { userAPI } from './user-api';



export interface AuthState  {
    user?: User | null 
    token?: string | null 
}


export const slice = createSlice({
    name: 'auth',
    initialState: { user: null, token: null } as AuthState,
    reducers: {
        setCredentials: (
            state,
            { payload: { user, token } }: PayloadAction<AuthState>
        ) => {
            state.user = user
            state.token = token
            localStorage.setItem('token', String(token))
        },

        logout : (
            state) => {
            state.user = null;
            localStorage.removeItem('token');
        },
    },
    extraReducers: (builder) => {
        builder.addMatcher(
            userAPI.endpoints.getAccountUser.matchFulfilled,
            (state, { payload }) => {
                state.user = payload
            }
        )
    },

})

export const { setCredentials, logout } = slice.actions

export default slice.reducer

export const selectCurrentUser = (state: RootState) => state.auth.user