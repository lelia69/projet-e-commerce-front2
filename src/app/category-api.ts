import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Category, Product } from '../entities';
import { prepare } from './token';


export const categoryAPI = createApi({
    reducerPath: 'categoryAPI',
    tagTypes: ['CategoryList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/category', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getAllCategory: builder.query<Category[], void>({
            query: () =>  '/',
            providesTags: ['CategoryList']
        }),
        getOneCategory: builder.query<Category, number>({
            query: (id) => '/'+id
        }),
        getProductCategory: builder.query<Product[], number>({
            query: (id) => '/product/'+id
        }),
        postCategory: builder.mutation<Category, Category>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['CategoryList']
        }),
        patchCategory: builder.mutation< Category, {body: Category, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['CategoryList']
        }),
        deleteCategory: builder.mutation<void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['CategoryList']
        })

    })
});


export const {useGetAllCategoryQuery, useLazyGetProductCategoryQuery, useGetProductCategoryQuery, usePostCategoryMutation, usePatchCategoryMutation, useDeleteCategoryMutation, useGetOneCategoryQuery} = categoryAPI;


