import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { orderAPI } from './order-api';
import { productAPI } from './product-api';
import { userAPI } from './user-api';
import { categoryAPI } from './category-api';
import authSlice from './auth-slice';
import  cardSlice  from './card-slice';


export const store = configureStore({
  reducer: {
    [productAPI.reducerPath]:productAPI.reducer,
    [userAPI.reducerPath]:userAPI.reducer,
    [orderAPI.reducerPath]:orderAPI.reducer,
    [categoryAPI.reducerPath]:categoryAPI.reducer,
    auth:authSlice,
    cartslice: cardSlice,

  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(productAPI.middleware, userAPI.middleware, orderAPI.middleware, categoryAPI.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
