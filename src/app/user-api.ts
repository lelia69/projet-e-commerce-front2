import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { User } from '../entities';
import { AuthState } from './auth-slice';
import { prepare } from './token';


export const userAPI = createApi({
    reducerPath: 'userAPI',
    tagTypes: ['UserList'],

    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/user', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllUsers: builder.query<User[], void>({
            query: () =>  '/',
            providesTags: ['UserList']
        }),

        getOneUser: builder.query<User, number>({
            query: (id) => '/'+id
        }),

        getAccountUser: builder.query<User, void>({
            query: () =>  '/account',
            providesTags: ['UserList']
        }),

        postUser: builder.mutation<AuthState, User>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['UserList']
        }),

        postLoginUser: builder.mutation< AuthState, User>({
            query: (body) => ({
                url: '/login',
                method: 'POST',
                body
            }),
            invalidatesTags: ['UserList']
        }),

        patchUser: builder.mutation< User, {body: User, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['UserList']
        }),

        deleteUser: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['UserList']
        })

    })
});


export const {useGetAllUsersQuery, useGetAccountUserQuery ,usePostUserMutation, usePostLoginUserMutation, usePatchUserMutation, useDeleteUserMutation, useGetOneUserQuery} = userAPI;


