import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Product } from '../entities';
import { prepare } from './token';


export const productAPI = createApi({
    reducerPath: 'productAPI',
    tagTypes: ['ProductList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/product', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllProducts: builder.query<Product[], void>({
            query: () =>  '/',
            providesTags: ['ProductList']
        }),
        getOneProduct: builder.query<Product, number>({
            query: (id) => '/'+id
        }),
        getSearchedProduct: builder.query<Product[], any>({
            query: (val) => ({
                url: '/search/'+ val,
                method: 'GET',
            }),
           
        }),
        postProduct: builder.mutation<Product, Product>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['ProductList']
        }),
        patchProduct: builder.mutation< Product, {body: Product, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'PATCH',
                body: param.body
            }),
            invalidatesTags: ['ProductList']
        }),
        deleteProduct: builder.mutation< void, number>({
            query: (id)=>({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['ProductList']
        })

    })
});


export const {useGetAllProductsQuery, useGetSearchedProductQuery, usePostProductMutation, usePatchProductMutation, useDeleteProductMutation, useGetOneProductQuery} = productAPI;


