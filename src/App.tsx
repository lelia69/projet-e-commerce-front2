import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Footer from './Components/Footer';
import Navbar from './Components/Navbar';
import AccountPage from './pages/AccountPage';
import AdminPage from './pages/AdminPage';
import ContactPage from './pages/ContactPage';
import HomePage from './pages/HomePage';
import { OrderPage } from './pages/OrderPage';
import ProductDetailPage from './pages/ProductDetailPage';

export interface RouterParams{
  id: string
}
function App() {
  return (
    <BrowserRouter>
    <div>
      <Navbar />
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/account">
         <AccountPage />
        </Route>
        <Route path="/admin">
         <AdminPage />
        </Route>
        <Route path="/contact">
         <ContactPage />
        </Route>
        <Route path="/product/:id">
         <ProductDetailPage />
        </Route>
        <Route path="/order/:id">
         <OrderPage />
        </Route>
      </Switch>
      <Footer />
      </div>
      
      </BrowserRouter>
  );
}

export default App;
