import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import './footer.css';
import { Link } from 'react-router-dom';



export default function Footer() {

    return (
        <div className="container1" >
            <div className="container">

                <article className="article1">
                    <section>
                        <h3>Reseaux Sociaux</h3>
                        <p >
                            <FacebookIcon fontSize="medium" style={{ marginRight: "10%", color: "rgb(66, 103, 178)" }} />
                            <TwitterIcon style={{ marginRight: "10%", color: "rgb(29, 161, 242)" }} />
                            <InstagramIcon style={{ color: "orangered" }} />
                        </p>
                    </section>
                </article>


                <article className="article2">
                    <section>
                        <h3>Nous Contacter</h3>
                        <p>Une réclamation ? Des informations Supplémentaires ? Des félicitations ?</p>
                        <Link to="/contact" /*target='_blank'*/>
                            <Button color="error" variant="outlined" href="#contained-buttons" disableElevation endIcon={<SendIcon />}>
                                Contact
                        </Button>
                        </Link>

                    </section>
                </article>



            </div>



        </div>



    )
}