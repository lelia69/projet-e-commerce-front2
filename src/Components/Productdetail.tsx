import { useParams } from "react-router-dom";
import { RouterParams } from "../App";
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import React from "react";
import './Productdetail.css';
import { useGetOneProductQuery } from "../app/product-api";



const ExpandMore = styled<any>((props:any) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
  })(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }));
  
  export default function Productdetail() {
    const [expanded, setExpanded] = React.useState(false);
  
    const handleExpandClick = () => {
      setExpanded(!expanded);
    };



    const { id } = useParams<RouterParams>();
    console.log (id)

   const { data,  } = useGetOneProductQuery(Number(id));

    return (
     
      
        <div className="Nasrine">

          <div className="photo"></div>

          <article style={{display:'flex', alignItems:'center', justifyContent:'space-evenly', width:'100%'}}>

                      <section>
                      <CardMedia
                      style={{borderBottom:'1px solid orangered',borderTop:'1px solid orangered'}}
                      component="img"
                      height="300"
                      image={data?.picture}
                      alt="texte"
                      
                    />
                      </section>

                      <section  >

                        <Card sx={{ width: 345 }}>
                    <CardHeader
                    
                      style={{borderBottom:'1px solid orangered'}}
                      title={data?.name}
                      subheader={data?.brand}
                    />
                    <CardContent style={{textAlign:'end'}}>
                      <Typography variant="body2" color="">
                      {data?.price}€
                      </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                      <IconButton aria-label="add to favorites">
                        <FavoriteIcon />
                      </IconButton>
                      
                      <ExpandMore
                        expand={expanded}
                        onClick={handleExpandClick}
                        aria-expanded={expanded}
                        aria-label="show more"
                      >
                        <ExpandMoreIcon />
                      </ExpandMore>
                    </CardActions>
                    <Collapse in={expanded} timeout="auto" unmountOnExit>
                      <CardContent>
                        <Typography paragraph>{data?.description}</Typography>
                      
                      </CardContent>
                    </Collapse>
                  </Card>
                      </section>

          </article>
        
      </div>


    );
  }
  






  
 /* <div className="container">

  <div className="row mt-5 justify-content">

      {!isLoading && data ?
          <>
              <div className="col-6">
                  <img src={data.picture} className="img" alt={data.name} />
              </div>

              <div className="col-6">
                  <h5 className="name">{data?.name}</h5>
                  <p className="brande">Category: {data?.brand}</p>
                  <p className="description">Description: {data?.description}</p>
                  <p className="price">Price: {data?.price}€</p>
      
                  
              </div>

          </>
          : <p>Chargement en cours...</p>}
  </div>
</div>

)
}*/