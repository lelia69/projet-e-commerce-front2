
import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LoginForm from './LoginForm';
import { useAppSelector } from '../app/hooks';
import { Typography } from '@mui/material';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  borderTop: '2px solid orangered',
  borderBottom: '1px solid orangered',
  boxShadow: 24,
  p: 4,
};



export default function ModalLogin() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  let user = useAppSelector(state => state.auth.user)

  return (
    <div>
      <Button onClick={handleOpen}><AccountCircle style={{ marginLeft: '10px', color:"whitesmoke" }} fontSize="large" /></Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
            {!user ? <LoginForm /> : <Typography style={{textAlign:'center', fontFamily:'Rajdhani, sans-serif', fontWeight:'bolder', letterSpacing:'3px', fontSize:'larger'}}>Welcome in [GETLENS]</Typography>}
        </Box>
      </Modal>
    </div>
  );
}