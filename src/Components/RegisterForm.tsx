import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { usePostUserMutation } from '../app/user-api';
import { User } from '../entities';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { setCredentials } from '../app/auth-slice';
import blueGrey from '@material-ui/core/colors/blueGrey';



function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: 'whitesmoke',
    border:'1px solid orangered'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    height:'50%',
    
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: blueGrey[900]
  },
}));

export default function RegisterForm() {
  const classes = useStyles();

  
  let user = useAppSelector(state => state.auth.user)

  const [postUser] = usePostUserMutation();
  const [form, setForm] = useState<User>({} as User);
  let dispatch = useAppDispatch()

  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    const data = await postUser(form).unwrap()
    if (data) {
      dispatch(setCredentials(data))
    }
  }

  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }


    setForm(change)
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon style={{color: 'orangered'}} />
        </Avatar>
        <Typography component="h1" variant="h5">
          BIENVENUE </Typography>
        <form className={classes.form} onSubmit= {handleSubmit}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="lname"
                name="name"
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Nom"
                autoFocus
              onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>

              <TextField
                variant="outlined"
                required
                fullWidth
                id="firstname"
                label="Prénom"
                name="firstname"
                autoComplete="fname"
              onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12}></Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                type="email"
                required
                fullWidth
                id="email"
                label="Email "
                name="email"
                autoComplete="email"
              onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>

              <TextField
                variant="outlined"
                fullWidth
                name="phone"
                label="Telephone"
                type="tel"
                id="phone"
                autoComplete="tel"
              onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="adress"
                label="Adresse"
                type="text"
                id="adress"
                autoComplete="text"
              onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Mot de passe"
                type="password"
                id="password"
                autoComplete="current-password"
                inputProps={{ minLength: 6 }}

              onChange={handleChange}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Créer mon compte
          </Button >
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="#" variant="body2">
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}