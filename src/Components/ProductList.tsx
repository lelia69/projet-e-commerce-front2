import { useGetAllProductsQuery } from "../app/product-api";
import { Category, Product } from "../entities";
import OneProduct from "./OneProduct";
import Grid from '@mui/material/Grid';
import { useGetProductCategoryQuery } from "../app/category-api";



interface Props {
category? : Category;
}

 function useFecthChoice(category?: Category):any {

  if (category?.id) {

    return useGetProductCategoryQuery.bind(null, category.id)  
  }

  return useGetAllProductsQuery
 }


export default function ProductList({category} : Props){
  
const {data, isLoading, isError} = useFecthChoice(category)()
  
  
  if (isLoading) {
      return <p>Loading...</p>
  }
  if(isError){
    return <p>{isError}</p>
  }

  return (
    
        <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 4, sm: 4, md: 16}}>
          {data?.map((item: Product) => (
            <Grid item xs={12} sm={8} md={5} key={item.id}>

              <OneProduct product={item}/>
            </Grid>
          ))}
        </Grid>
 

  );
}