import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useGetOneOrderQuery } from '../app/order-api';
import './OrderList.css';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { CardMedia } from '@material-ui/core';





// Style Tableau
const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

interface Props {
    id: number
}

export default function OrderList({ id }: Props) {


    const { data } = useGetOneOrderQuery(id)
    console.log(data);




    return (

        <div className="contains">

            <article>

                <section>

                    <TableContainer sx={{ width: 600 }} component={Paper}>
                        <Table aria-label="customized table">
                            <TableHead>
                                <TableRow>

                                    <StyledTableCell align="right">Quantité</StyledTableCell>
                                    <StyledTableCell align="right">Prix Unitaire</StyledTableCell>
                                    <StyledTableCell align="right">Produit</StyledTableCell>
                                    <StyledTableCell align="right"></StyledTableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>

                                {data?.productlines?.map(item => {
                                    return <StyledTableRow key={item.id}>

                                        <StyledTableCell align="right">{item.quantity}</StyledTableCell>
                                        <StyledTableCell align="right">{item.price}€</StyledTableCell>
                                        <StyledTableCell align="right">{item.brand}</StyledTableCell>
                                        <StyledTableCell align="right"><CardMedia style={{ objectFit: "fill", width: '100px', height: '100px', borderTop: '1px solid orangered', borderBottom: '1px solid orangered' }}
                                            image={item.picture}></CardMedia></StyledTableCell>

                                    </StyledTableRow>
                                })}

                            </TableBody>
                        </Table>
                    </TableContainer>

                </section>


                <section style={{ marginLeft: '-8%' }}>
                    <Card sx={{ minWidth: 275 }} key={data?.id}>
                        <CardContent style={{ borderTop: '1px solid orangered', backgroundColor: 'rgba(224, 224, 224, 0.5)' }}>
                            <Typography sx={{ mb: 1.5, borderBottom: '1px solid orangered', padding: '10px', fontFamily: 'Rajdhani, sans-serif', fontWeight: 'bold', fontSize: 'large', color: 'black' }} color="text.secondary">
                                Numéro client <span>-------</span> {data?.users?.id}
                            </Typography>

                            <Typography sx={{ mb: 1.5, borderBottom: '1px solid black', padding: '10px', fontFamily: 'Rajdhani, sans-serif', fontWeight: 'bold', fontSize: 'large' }} color="text.secondary">
                                {data?.users?.name} {data?.users?.firstname}
                            </Typography>

                            <Typography sx={{ mb: 1.5, borderBottom: '1px solid black', padding: '10px', fontFamily: 'Rajdhani, sans-serif', fontWeight: 'bold', fontSize: 'large' }} color="text.secondary">
                                {data?.adress}
                            </Typography>
                            <br />
                            <Typography style={{ fontFamily: 'Rajdhani, sans-serif', fontWeight: 'bold' }} variant="h5" component="div">
                                Total  : <span style={{ marginLeft: '15%' }}>{data?.total}€</span>
                            </Typography>

                        </CardContent>
                        <CardActions style={{ backgroundColor: 'rgba(224, 224, 224, 0.5)' }}>
                            <Button style={{ backgroundColor: '#263238', letterSpacing: '2px', borderBottom: '1px solid orangered', padding: '8px' }} size="small" variant="contained" disableElevation>Finaliser ma commande</Button>
                        </CardActions>
                    </Card>

                </section>

            </article>

        </div>


    );
}