import React, { useState } from 'react';
import { alpha, makeStyles, Theme, createStyles, styled } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import blueGrey from '@material-ui/core/colors/blueGrey';
import Panier from './Panier';
import ModalLogin from './ModalLogin';
import ModalRegister from './ModalRegister';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { Button } from '@mui/material';
import { logout } from '../app/auth-slice';
import ModalSearchedProd from './ModalSearchedProd';
import { useHistory } from 'react-router';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';



// style SEARCHBAR
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
      color:'whitesmoke',
      
      
    },
    app: {

      backgroundColor: blueGrey[900],
      borderBottom: '1px solid orangered'
    },

    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: alpha(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
      borderBottom: '1px dashed orangered'
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
);


// style TOOLTIP
const BootstrapTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} arrow classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: theme.palette.common.black,
  },
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.black,
  },
}));




export default function Navbar() {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null);
  

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  let user = useAppSelector(state => state.auth.user)
  let dispatch = useAppDispatch()

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };



  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >


      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
      </MenuItem>
    </Menu>
  );
  // Hook who menage the state of the imput value
  const [searchedVal, setSearchedVal]= useState("")

  //Function that stock input value in the variable searchVal
   const handleChange = (event:any)=> {
        setSearchedVal(event.target.value);
   }

// Redirection
   let history = useHistory();

   function handleClick(){
     //If loged user is administrator then redirect him to admin page ELSE redirect to user account page
     if(user?.role==="admin"){
      history.push("/admin")
     }else{
       history.push("/account")
     }
      
    }
  
  return (
    <div className={classes.grow}>
      <AppBar className={classes.app} position="static">
        <Toolbar>
          <Link style={{textDecoration:"none"}} to="/">
            <Typography className={classes.title} variant="h6" noWrap>
              [GETLENS]
          </Typography>
          </Link>

          {/* Search Bar */}
          <div className={classes.search} style={{display: 'flex', alignItems: 'flex-start'}}>
           {/* I need to get input value from here */}
            <InputBase
              placeholder="Search…"
              onChange={handleChange}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
            
            <ModalSearchedProd val={searchedVal}/>
          </div>


          <div className={classes.grow} />
          <Panier />
                  {/* User Interface */}
                  {user ?
          <> 
            <Button onClick={handleClick}>  <BootstrapTooltip  title="Mon compte"><AccountCircle style={{ marginRight: '60%', color:"whitesmoke" }} fontSize="large" /></BootstrapTooltip>   </Button>
            <Button style={{ color: 'white', borderLeft: '2px solid orangered', borderRight: '2px solid orangered',borderTop:'none',borderBottom:'none', marginRight: '20px', letterSpacing:'2px' }} variant="outlined" disableElevation  size="medium" onClick={() => dispatch(logout())} >
            Logout </Button>
          </>
          
          :
          <><ModalRegister /><ModalLogin /></>}
    
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}