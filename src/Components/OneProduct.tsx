import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { Product } from '../entities';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import { useAppDispatch, useAppSelector,  } from '../app/hooks';
import { addCart } from '../app/card-slice';
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxWidth: 300,
            backgroundColor: 'rgb(15,20,22, 0.2)',
            margin: 15,
        },
        media: {
            objectFit: "fill",
            paddingTop: '48.25%', // 16:9
            borderBottom: '1px solid orangered',
            borderTop: '1px solid orangered',

        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        },
        expandOpen: {
            transform: 'rotate(180deg)',
        },

        actions: {
            height: "60px"
        },

    }),
);

interface Props {
    product: Product;
}



export default function OneProduct({ product }: Props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const cart = useAppSelector(state => state.cartslice.cart)
    const dispatch = useAppDispatch();


    /**
     * Fonction qui permet d'ajouter les produits au panier
     */
    const addNewProduct = ()=>{
        
        dispatch(addCart(product))
        
        console.log(product);
        
    }


    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <>
        
              
          
        <Card elevation={3} className={classes.root}>
            <CardHeader
                title={product.name}
                subheader={product.brand}
            />
            <CardMedia
                className={classes.media}
                image={product.picture}
            />
            <CardContent >
                <Typography variant="body2" color="textSecondary" component="p">
                    stock = {product.stock}
                </Typography>
            </CardContent>
            <CardActions className={classes.actions} disableSpacing>
                <Typography variant="body2" color="textSecondary" component="p">
                    {product.price}€
                </Typography>
                <Link to={'/product/'+ product.id}>
                <IconButton aria-label="visibility">
                    <Tooltip title="Voir" arrow placement="right-start">
                        <Button><VisibilityIcon fontSize='large' style={{ color: "orangered" }} /></Button>
                    </Tooltip>

                </IconButton>
                </Link>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="add"
                >
                    <Fab aria-label="add">
                        <AddIcon onClick={() => addNewProduct()} />
                    </Fab>

                </IconButton>
            </CardActions>
        </Card>
        </>
    );
}
