import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import SearchIcon from '@material-ui/icons/Search';
import { useGetSearchedProductQuery } from '../app/product-api';
import { Grid } from '@mui/material';
import OneProduct from './OneProduct';
import { Product } from '../entities';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '70%',
  bgcolor: 'background.paper',
  borderTop: '2px solid orangered',
  borderBottom: '1px solid orangered',
  boxShadow: 24,
  p: 4,
};


interface Props {
    val? : String;
    }
export default function ModalSearchedProd({val}: Props) {

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  const {data, isLoading, isError} = useGetSearchedProductQuery(val)
  
  
  if (isLoading) {
      return <p>Loading...</p>
  }
  if(isError){
    return <p>{isError}</p>
  }

  return (
    <div>
      <Button onClick={handleOpen}><SearchIcon style={{ color:"whitesmoke" }} fontSize="medium" /></Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
            {data?
                    <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 4, sm: 4, md: 16}}>
                        {data?.map((item: Product) => (
                            <Grid item xs={12} sm={8} md={5} key={item.id}>
                            <OneProduct product={item}/>
                            </Grid>
                        ))}
                    </Grid>
                        :
                    <p>You need to type something in the search bar</p>
        }
            


        </Box>
      </Modal>
    </div>
  );
}