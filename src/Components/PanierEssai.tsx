import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import { Card } from '@material-ui/core';
import { Button } from '@mui/material';
import DoneIcon from '@mui/icons-material/Done';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { usePostOrderMutation } from '../app/order-api';
import {  Redirect } from 'react-router-dom';
import { deleteCart, replaceCart } from '../app/card-slice';



export default function PanierEssai() {

    /**
     * On récupère les data du panier
     */
    const cart = useAppSelector(state => state.cartslice.cart)

    const [postOrder, { data }] = usePostOrderMutation();
    const dispatch = useAppDispatch()

    /**
     * Fonction qui permet de vider le panier lorsque l'on passe à la commande
     */
    const deletePanier = () => {

        dispatch(deleteCart(cart))

    }


    /**
     * Fonction qui permet de créer une nouvelle commande à base des data dans le panier
     */
    async function goToOrder() {
        let data = await postOrder(cart).unwrap()



    }

    /**
     * 
     * @param id Fonction qui permet de supprimer un article dans le panier
     */
    const deleteOne = (id: number) => {

        dispatch(replaceCart(cart.filter(item => item.id !== id)))
    }



    return (
        

        !data ? <Card style={{ backgroundColor: 'rgb(255,255,255,0.9', width: '100%' }}>

            <Typography style={{ textAlign: 'center' }} sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
                Mon Panier
          </Typography>




            {cart.map((item, index) => {

                return <List key={item.id} >
                    <ListItem
                        secondaryAction={
                            <IconButton onClick={() => deleteOne(item.id!)} edge="end" aria-label="delete">
                                <DeleteIcon style={{ color: "red" }} />
                            </IconButton>
                        }
                    >
                        <ListItemAvatar>
                            <DoneIcon style={{ color: 'grey' }} />
                        </ListItemAvatar>
                        <ListItemText
                            primary={item.name}

                        />


                    </ListItem>

                </List>
            })}


            <Button onClick={() => {goToOrder(); deletePanier()}} style={{ marginLeft:'7%', marginTop: '2%', backgroundColor: '#263238', borderLeft: '2px solid orangered', borderRight: '2px solid orangered' }} variant="contained" disableElevation>
                Commander
                </Button>






            )





        </Card> : <Redirect to={'/order/' + data.id} />



    );
}