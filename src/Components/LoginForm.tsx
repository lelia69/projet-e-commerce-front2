import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Redirect } from "react-router";
import { User } from "../entities";
import { usePostLoginUserMutation } from '../app/user-api';
import { setCredentials } from '../app/auth-slice';
import { useAppDispatch, useAppSelector } from '../app/hooks';



const theme = createTheme();

export default function LoginForm() {
  

  let user = useAppSelector(state => state.auth.user)

  const [postLogin, postQuery] = usePostLoginUserMutation();
  const [form, setForm] = useState<User>({} as User);
  let dispatch = useAppDispatch()

  const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    const data = await postLogin(form).unwrap()
    if (data) {
      dispatch(setCredentials(data))
    }
  }
  
  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }


    setForm(change)
  }



  return (!user ?
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: '#263238' }}>

          </Avatar>
          <Typography component="h1" variant="h5">
            GETLENS          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email "
              name="email"
              autoComplete="email"
              autoFocus
              onChange={handleChange}

            />
            <TextField
             variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={handleChange}

            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="outlined"
              sx={{ mt: 3, mb: 2, color:'orangered', border:'2px solid orangered', fontSize:'large' }}
              
              
            >
              Se Connecter
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">

                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">

                </Link>
              </Grid>
            </Grid>
          </Box>
          {postQuery.isError && <h5 className='text-center text-uppercase text-white bg-danger py-2 m-1' >{(postQuery.error as any).data.error}</h5>}
        </Box>
      </Container>
    </ThemeProvider> : <Redirect to='/' />
  );
}