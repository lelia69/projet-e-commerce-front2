
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import { useGetOneUserQuery, usePatchUserMutation } from '../../app/user-api';
import { User } from '../../entities';
import TextField from "@material-ui/core/TextField";
import { useState } from 'react';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '70%',
  bgcolor: 'background.paper',
  borderTop: '2px solid orangered',
  borderBottom: '1px solid orangered',
  boxShadow: 24,
  p: 4,
};


interface Props {
    user : User;
    }
export default function ModalUserWindow({user}: Props) {

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  const {data, isLoading, isError} = useGetOneUserQuery(Number(user?.id))

  const [form, setForm] = useState<User>({} as User);
  const [postUser, postQuery]= usePatchUserMutation()

  const handleSubmit = async (event: React.FormEvent<EventTarget>)=>{
        event.preventDefault();
     
            await postUser({body:form, id:user.id!}).unwrap();
            alert("User updated successfully")
  }

  const handleChange = (event: React.FormEvent<EventTarget>) => {
    let target = event.target as HTMLInputElement;
    let name = target.name;
    let value = target.value
    let change = { ...form, [name]: value }
    setForm(change)
  }
  
  
  if (isLoading) {
      return <p>Loading...</p>
  }
  if(isError){
    return <p>{isError}</p>
  }

  return (
    <div>
      <Button onClick={handleOpen}>User details</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
            <p>Id: {data?.id} |  Name: {data?.name}</p>
           <TextField style={{display: "block"}} placeholder={data?.email} label="Email" type="email" name="email"  onChange={handleChange}/>
           <TextField style={{display: "block"}} placeholder={data?.firstname} label="First Name" type="text" name="firstname" onChange={handleChange} />
           <TextField style={{display: "block"}} placeholder={data?.name} label="Name" type="text" name="name" onChange={handleChange} />
           <TextField style={{display: "block"}} placeholder={String(data?.phone)} label="Phone" type="phone" name="phone"  onChange={handleChange}/>
           <TextField style={{display: "block"}} placeholder={data?.role} label="Role" type="text" name="role" onChange={handleChange} />
           <Button onClick={handleSubmit}>Submit</Button>
        </Box>
      </Modal>
    </div>
  );
}