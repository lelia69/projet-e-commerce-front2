import { useGetAllUsersQuery } from "../../app/user-api";
import { User } from "../../entities";
import OneUser from "./OneUser";
import Grid from '@mui/material/Grid';





export function UserList(){

    const {data, isLoading, isError} = useGetAllUsersQuery()

    if(isLoading){
        return <p>Loading ...</p>
    }
    if(isError){
        return <p>{isError}</p>
    }

    return(
        <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 4, sm: 4, md: 16}}>
        {data?.map((item: User) => (
          <Grid item xs={12} sm={8} md={5} key={item.id}>

            <OneUser user={item}/>
          </Grid>
        ))}
      </Grid>
    )
}