import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { User } from '../../entities';
import ModalUserWindow from './ModalUserWindow';





interface Props {
    user: User
}
export default function OneUser({user}:Props) {

  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          {user.firstname}
        </Typography>
        <Typography variant="h5" component="div">
          {user.name}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {user.email}
        </Typography>
        <Typography variant="body2">
          {user.orders?.map(item=> <>{item.id}</>)}
          <br />
          {user.adress}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small"><ModalUserWindow user={user} /></Button>
        <Button size="small">Orders</Button>
      </CardActions>
    </Card>
  );
}