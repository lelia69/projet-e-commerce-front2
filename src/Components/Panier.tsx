import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import ShoppingBasketSharpIcon from '@material-ui/icons/ShoppingBasketSharp';
import Zoom from '@mui/material/Zoom';
import PanierEssai from './PanierEssai';

/**
 * Le Tooltip contient la liste de produits du panier
 */
const longText=  <PanierEssai />


export default function Panier() {

  

  return (

    <div>
      <Tooltip TransitionComponent={Zoom} title={longText} >
      
        <Button sx={{ m: 1 }}><ShoppingBasketSharpIcon style={{ marginRight: '20px', color: 'orangered' }} fontSize="large" /></Button>
      </Tooltip>
    </div>
  );
}