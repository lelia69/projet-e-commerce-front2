import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import HdrStrongIcon from '@material-ui/icons/HdrStrong';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import { Category } from '../entities';
import { useGetAllCategoryQuery } from '../app/category-api';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: 220,
            backgroundColor: "whitesmoke",
            borderRight: '1px dashed #263238',

        },
        nested: {
            paddingLeft: theme.spacing(4),

        },
    }),
);

interface Props {
    onSelect: Function;
}

export default function Sidebar({ onSelect }: Props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);

    const handleClick = () => {
        setOpen(!open);
    };


    const { data, isLoading, isError } = useGetAllCategoryQuery();

    let reload = ()=>{
        window.location.reload()
    }

    if (isLoading) {
        return <p>Loading...</p>
    }
    if (isError) {
        return <p>{isError}</p>
    }

    return (

        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={

                <ListSubheader  style={{backgroundColor: "#263238", color: "white", cursor: 'pointer'}} component="div" id="nested-list-subheader" onClick={reload}>
                    Tous nos Produits<ArrowRightAltIcon style={{ color: "orangered" }} fontSize="large" />
                </ListSubheader>

            }
            className={classes.root}
        >

            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    <HdrStrongIcon />
                </ListItemIcon>
                <ListItemText primary="Catégories" />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>

                    {data?.map((item: Category) => (



                        <ListItem button className={classes.nested} key={item.id} onClick={() => onSelect(item)}>
                            <ListItemIcon >
                                <ArrowRightIcon style={{ color: 'orangered' }} />
                            </ListItemIcon>
                            <ListItemText primary={item.label} />


                        </ListItem>



                    ))
                    }

                </List>
            </Collapse>

        </List>


    );
}
