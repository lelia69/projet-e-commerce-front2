
import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import RegisterForm from './RegisterForm';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height: 600,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

export default function ModalRegister() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button  onClick={handleOpen} style={{ color: 'white', borderLeft: '2px solid orangered', borderRight: '2px solid orangered',borderTop:'none',borderBottom:'none', marginRight: '20px' }} variant="outlined" disableElevation>
            Sign in
          </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <RegisterForm />
        </Box>
      </Modal>
    </div>
  );
}