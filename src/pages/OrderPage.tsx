import { useParams } from "react-router";
import { RouterParams } from "../App";
import OrderList from "../Components/OrderList";




export function OrderPage() {
    const {id} = useParams<RouterParams>()
    return (
        <div>
           <OrderList id={Number(id)}/>
        </div>
    )

}