import Sidebar from "../Components/Sidebar";
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import { ListItemIcon, makeStyles } from "@material-ui/core";
import HdrStrongIcon from '@material-ui/icons/HdrStrong';
import ProductList from "../Components/ProductList";
import { useState } from "react";
import { Category } from "../entities";


const useStyles = makeStyles({
    slider: {
        width: 180,
        marginTop: 60,
        borderLeft: ' 1px solid orangered',
        borderRight: ' 1px solid orangered',
        marginLeft: '20px'

    },
});
export default function HomePage() {


    const [cat,Setcat]=  useState<Category>();

    function valuetext(value: number) {
        return `${value}°C`;
    }

    const classes = useStyles();
    return (
        <div className="contain" style={{ backgroundColor: "whitesmoke", paddingTop: "3%" }}>

            <div style={{ display: "flex", backgroundColor: "whitesmoke", height: "100%" }}>
                <div>

                    <Sidebar onSelect={Setcat}  
                    />
                   {/* why cat is undefined ?  */}
                   {console.log(cat)}

                    <Typography style={{ marginTop: '100px', marginLeft: '15px' }} id="discrete-slider" gutterBottom>
                        <ListItemIcon>
                            <HdrStrongIcon />
                        </ListItemIcon>
                    Filtrer par prix
                </Typography>


                    <Slider className={classes.slider}
                        defaultValue={30}
                        getAriaValueText={valuetext}
                        aria-labelledby="discrete-slider"
                        valueLabelDisplay="auto"
                        step={10}
                        marks
                        min={10}
                        max={110}
                        disabled
                    />
                </div>

                <>
                    <ProductList category = {cat}/>

                </>

            </div>
        </div>


    )
}