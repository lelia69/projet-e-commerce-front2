export interface User{
    id?:number;
    firstname?:string;
    name?:string;
    email?:string;
    password?:string;
    adress?: string;
    phone?: number;
    role?: string;
    orders?:Order[];
}

export interface Order {
    id?:number;
    date?:string; 
    status?:string;
    adress?:string;
    total?:number;
    users?:User;
    productlines?:ProductLine[];
}

export interface ProductLine {
    id?:number;
    quantity?:number;
    price?:number;
    products?:Product;
    orders?:Order[];
    brand?:string;
    picture?:string;
}


export interface Product {
    id?:number;
    name?:string;
    brand?:string;
    description?:string;
    price?:number;
    picture?:string;
    stock?:number;
    categories?:Category[];
    productlines?:ProductLine[];
}

export interface Category {
    id?:number;
    label?:string;
    products?:Product[];
}

